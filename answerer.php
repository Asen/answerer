<?php

/* Enabling neat answer and ability Ajax crossdomain communication */
header('Content-Type: text/html; charset=utf-8', true);
header("Access-Control-Allow-Origin: *", true);

/* Exit if no data */
if(!isset($_GET['data'])) 
	die('"Data" param wasnt specified!');

/* Errors watching(debug mode) */
ini_set('display_errors', 'On');
error_reporting(E_ALL);

mb_internal_encoding("UTF8");
define('ENV_FLD', '');

/* Utilities */
include ENV_FLD.'utils/question.php';
include ENV_FLD.'utils/curlfunc.php';
include ENV_FLD.'utils/simple_html_dom.php';
include ENV_FLD.'utils/utils.php';


function __autoload($class)
{
	require_once ENV_FLD.'interfaces/'.strtolower(trim($class)).'.php';
	}
	
	
function CombineData($array_of_array)
{
	$estimation = 0;
	$max_data_pieces = 0;
	
	foreach($array_of_array as $array){
		if( count($array['data']) > $max_data_pieces ) 
			$max_data_pieces = count($array['data']);
		$estimation += (float)$array['estimation'];
	}
	
	$output = array();
	
	for($j=0; $j<$max_data_pieces; ++$j)
		for($i=0; $i<count($array_of_array); ++$i)
			if(isset($array_of_array[$i]['data'][$j]))
				array_push($output, $array_of_array[$i]['data'][$j]);	
	
	return array('key'=>'otvet', 'data'=>$output, 'estimation'=>$estimation);
	
	}
	
function DecorateOutput($output)
{
	$decorated = '';
	foreach($output['data'] as $data_piece)
		$decorated .= '<div class="a-a"><div class="a-a-c">'.$data_piece.'</div></div>';
		
	die($decorated);
	}


function HandleInput()
{
	########################################################
	$user_input = trim($_GET['data']);
	########################################################

	/* Priority: 1 */
	$output = CombineData( array(Nigma::getData($user_input), Wolphram::getData($user_input)) );
	if($output['estimation']>0) 
		return DecorateOutput($output);
		
	/* Priority: 2 */
	$bundle = Mail::getData($user_input);
	$output = CombineData( array($bundle, MailOtvet::getData($user_input, $bundle["forward"])) );
	if($output['estimation']>0) 
		return DecorateOutput($output);
	}


/* Compuation entry point */
HandleInput();


?>