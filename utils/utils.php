<?php 

class utils{
	
	public static function isAnyRussianLetter($string)
	{
		$RU = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
		$string = trim(mb_strtolower($string));
		return mb_strlen(strpbrk($string,$RU))>0;
		}
	
	public static function RemoveTrash($text, $trash = NULL)
	{
		if( $trash==NULL || !is_array($trash) ) 
		$trash = array();
		
		array_push($trash, '=)');
		array_push($trash, '))');
		array_push($trash, ':)');
		array_push($trash, ';)');
		array_push($trash, '=(');
		array_push($trash, '((');
		array_push($trash, ':(');
		array_push($trash, '%)');
		array_push($trash, 'эх,');
		array_push($trash, 'эй,');
		array_push($trash, 'ай,');
		array_push($trash, '>_<');
		array_push($trash, '??');
		array_push($trash, '!!');
		array_push($trash, 'бля');
		array_push($trash, 'хуй');
		array_push($trash, array('...','.'));
		
		$remover = array();
		foreach($trash as $key=>$val)
		{
			if( is_array($val) ){
				array_push($remover,  $val[1] );
				$trash[$key] = $val[0];
			} else array_push($remover, '');
		}
		
		return str_ireplace($trash,$remover,$text); 
		}
		
	public static function uToUTF8( $str ) 
	{
		$str = preg_replace('/\\\u([0-9a-fA-F]{3,4})/','&#x\1;', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		return $str;
	}
	
	
	public static function ReplaceLinks($text, $color='#00F')
	{
		return preg_replace('#((?:http|https):\/\/[^\s]+)#i',
		'<a href="$1" class="underlined" style="color:'.$color.'" target="_blank">$1</a>', $text);
		}
	
	
	}

?>