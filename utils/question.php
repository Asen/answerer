<?php

class question
{
	const UNDEFINED_Q = -1;
	const WHY_Q = 1;
	const WHERE_Q = 2;
	const HOW_Q = 3;
	const WHO_Q = 4;
	
	public static $WHY = array('почему','зачем','из-за');
	public static $WHERE = array('где','откуда');
	public static $HOW = array('как');
	public static $HWO = array('кто','кем');
	
	public static function defType($question_word)
	{
		$input = array(trim(mb_strtolower($question_word)));
		if( count(array_intersect($input, Question::$WHY)) > 0 ) return Question::WHY_Q;
		if( count(array_intersect($input, Question::$WHERE)) > 0 ) return Question::WHERE_Q;
		if( count(array_intersect($input, Question::$HOW)) > 0 ) return Question::HOW_Q;
		if( count(array_intersect($input, Question::$HWO)) > 0 ) return Question::WHO_Q;
		
		return Question::UNDEFINED_Q;
		
		}
	
	}

?>