<?php

class MailOtvet extends BasicData
{	
	private static $original_question = NULL;
	private static $questionType = NULL;
	private static $question_slices = NULL;
	private static $estimation = 0;

	public static function getData($query, $forwarding = array())
	{		
		/* Static replica */
		MailOtvet::$original_question = $query;
	
		/* Define question type */
		MailOtvet::$question_slices = array_map("MailOtvet::array_mapping", array_filter(explode(' ', $query)));
		if(MailOtvet::$question_slices == 0) return array();
		MailOtvet::$questionType = Question::defType(MailOtvet::$question_slices[0]);
		
		/* Get all appropriate questions in JSON fmt */
		$url = 'http://go.mail.ru/answer_json?num=15&q='.urlencode($query);
		$data = http_request($url, FALSE, '', 'http://otvet.mail.ru', FALSE, 30);
		$data = json_decode($data);
		$results = $data->results;
		$results = array_merge($forwarding, $results);
			
		$outlet = array();
		
		foreach($results as $question_object)
		{			
			/* If we got sufficient amount of answers then finish */
			if( count($outlet)>=5 ) 
				break;
					
			$questions_html = file_get_html($question_object->url);
			$answers = $questions_html->find('.atext');
			/* If no answers for question then continue */
			if( count($answers)==0 ) 
				continue;
			
			/* Accepting first(top) answer ONLY */
			$answer = trim($answers[0]->innertext);
			$answer = strip_tags($answer, "<img><b><u><i><p><br><===><=><==><====>");	
			if(isset($question_object->answer)){
				
				$signed_answer = strip_tags($question_object->answer);
				// Top answer in HTML 
				$a = trim(mb_strtolower(strip_tags($answer)));
				// Single(top) answer from JSON object
				$b = trim(mb_strtolower(strip_tags($signed_answer)));
				
				if( (mb_strlen($a)==0 || mb_strlen($b)==0) || (!mb_strstr($a, $b) && !mb_strstr($b, $a)))
					//$answer .= '<div class="little-border"></div>'.$signed_answer;
					array_push($outlet, MailOtvet::ProcessNewAnswer($signed_answer, $question_object));
					
				//echo $signed_answer."<br><br>";
			}
	
			//$answer .= "(mail)";
			array_push($outlet, MailOtvet::ProcessNewAnswer($answer, $question_object));
			
		}
		
		return MailOtvet::ReorderAnswers($outlet);
		
		}

	private static function ProcessNewAnswer($answer, $question_object)
	{
			/* Euristics filter */
			if( MailOtvet::$questionType==Question::WHY_Q && mb_strlen($answer)<30 ) continue;
			if( MailOtvet::$questionType==Question::HOW_Q && mb_strlen($answer)<30 ) continue;
			if( MailOtvet::$questionType==Question::WHO_Q && mb_strlen($answer)<8 )  continue;
			
			
			if(MailOtvet::$estimation == 0) 
				MailOtvet::$estimation = 1;
				
			
			if(mb_strlen($answer) >= 3){
				/* Appropriate question title */
				$question_title = trim(strip_tags($question_object->question));
				/* Length difference between source and @appropriate questions */
				$question_len_diff = abs( mb_strlen($question_title)-mb_strlen(MailOtvet::$original_question) );
				/* Counting word substrings of source question in appropriate one */
				$question_data = array_map("MailOtvet::array_mapping", array_filter(explode(' ', $question_title)));
				$source_in_app_substrs_count = count(array_intersect(MailOtvet::$question_slices, $question_data));
				
				if($source_in_app_substrs_count == count(MailOtvet::$question_slices)) 
					MailOtvet::$estimation++;
					
				if($source_in_app_substrs_count != 0){
					$local_answer_estimation = $source_in_app_substrs_count - $question_len_diff;
					return array($local_answer_estimation, $answer);
				}
			}	
	}

	private static function ReorderAnswers($answer_colelction)
	{
		function usorter($a, $b){
			/* [0] => local answer estimaation */ 
			return $a[0] < $b[0]; 
		}
		
		usort($answer_colelction, 'usorter');
		
		$final_output = array();
		foreach($answer_colelction as $answer_items) 
			array_push(
				$final_output, 
				Utils::RemoveTrash($answer_items[1], array('http://content.foto.mail.ru/mail/pilot-667/_answers/i-376.jpg')) 
				);
		
		return array('key'=>'mailotvet', 'data'=>$final_output, 'estimation'=>MailOtvet::$estimation);
	}
		
	private static function array_mapping($item)
	{
		$item = trim(mb_strtolower($item));
		$item_len = mb_strlen($item);
		if( Question::defType($item)==Question::UNDEFINED_Q && $item_len>4 ) 
			$item = mb_substr($item, 0, $item_len-2);
		return $item;
	}	
		
	
}

/*
;window["jQuery1111010442642238922417_1406975854426"]&&window["jQuery1111010442642238922417_1406975854426"]
;window["jQuery1111010442642238922417_1406975854426"]&&window["jQuery1111010442642238922417_1406975854426"]
http://go.mail.ru/answer_json?ajax_id=0&callback=jQuery1111010442642238922417_1406975854426&q=where&num=20&sf=0&salt=CSgNY7&token=bb214894c4abcb8bdf80859d3b0d9cd8081a231378f413f1ebdd445799aae994&_=1406975854427

http://go.mail.ru/answer_json?q=where&num=20
*/

?>