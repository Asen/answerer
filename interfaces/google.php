<?php

class Google extends BasicData
{
	public static function getData($query)
	{		
		$cookie = 'SID=DQAAANoAAAD8Wkni6W75iA_W4g9Io3ifknUmFQFqsHxBLsVfsuyM06udtRBFHJorsoL6EGfrXVNKZE5vJkdH7luoZq_mjkimBciy4t5VB9UoHpYNZWSTlVA6ikMIIoKIw8F8fFs3n7G7MNu5hoCdFj6AdpBTPLRcPg2KiVDNusAHBvrZOW5DZOPQ06KbN06EAr8-fcNHSoPDUjvLkjJt1EjudbtvAVj1AuXn25c_r6-GlHiv-sqRtz34-CyCynQYVDGYat460up8AlTVfDD-IKInT3AW-JwoXDJ4Yko2BtmmFIT_jA8GYg;HSID=AX_6R_K3SE7itsW7h;';			
		$post_token = '';

		$url = 'http://www.google.ru/search?q='.urlencode($query.' site:http://otvety.google.ru/');

		$html = http_request($url, FALSE, '', 'http://otvety.google.ru', $cookie, 30);
		$data = new simple_html_dom();
		$data->load($html);
		$hrefs = $data->find("a");
		
		$tids = array();
		foreach($hrefs as $href)
		{
			if( !isset($href->attr['href']) || !mb_strstr($href->attr['href'],'thread?tid=') ) continue;
			$h = explode('thread?tid=', $href->attr['href']);
			array_push($tids, $h[1]);
			if( count($tids)>=5 ) break;
			}

		$outlet = array();
		$est = 0;
		foreach($tids as $tid)
		{
			if($post_token=='')
			{
				$html = http_request('http://otvety.google.ru/otvety/thread?sort=wsmorv&tid='.$tid, FALSE, '', 
				'http://otvety.google.ru', $cookie, 30);
				
				$dom = new simple_html_dom();
				$dom->load( $html );
				$dom = $dom->find('input[name="POST_TOKEN"]');
				$post_token = $dom[0]->attr['value'];
				unset($html, $dom);
				}
			
			$post_data = 'command=17&data='.urlencode('["gansrq","'.$tid.'",null,[],1407155168717000,true]').'&POST_TOKEN='.$post_token;
			$html = http_request('http://otvety.google.ru/otvety/ajax', TRUE, $post_data, 'http://otvety.google.ru/otvety', $cookie, 30);
			//$html = stripslashes($html);
			$html = json_decode( $html );
			$html = $html[1];
			
			$offset = 0;
			$needle = '["post","';
			$i = 0;
			$MAX = $est==0 ? 2 : 1;
			while(true)
			{
				$found = mb_strpos($html, $needle, $offset);
				if( !$found || $found<1 || $i>=$MAX ) break;
				$found += mb_strlen($needle);
				$end = mb_strpos($html, ',[', $found) - 1;
				$offset = $end;
				$str = trim(mb_substr($html, $found, $end-$found));
				//$str = mb_substr($str, 0, 1000);
				$str = strip_tags($str,"<b><a><i><u>");
				$str = str_replace(array('\r','\n'), array(" ","<br>"), $str);
				$str = Utils::ReplaceLinks( Utils::RemoveTrash( trim(Utils::uToUTF8( $str )) ));
				$str = stripslashes( $str );
				if( mb_strlen($str)<7 ) continue;
				array_push($outlet, $str);
				//$outlet .= $str.'<br><br><hr><br>';
				$est++;
				$i++;
				}
			}
		
		return array('key'=>'googleotvet', 'data'=>$outlet, 'estimation'=>$est);
		
		}
	
	}

?>