<?php

class Wolphram extends BasicData
{	
	public static function getData($query)
	{
		/* Processing english input ONLY */
		if( Utils::isAnyRussianLetter($query) ) 
			return array('key'=>'wolphram', 'data'=>array(), 'estimation'=>0);
		
		$base_url = 'http://api.wolframalpha.com/v2/query?appid=X3UY42-R2AG8HRJ7W&input=';
		$url = $base_url.urlencode($query);			
		$html = file_get_html($url);
		
		$bundle = array();
		$is_smth_found = false;
		
		/* Take as a result images only */
		foreach($html->find("img") as $img)	
		{
			array_push($bundle, $img);
			$title = isset($img->attr['title']) ? $img->attr['title'] : 'zero';

			if( !$is_smth_found && mb_strlen($title)>0 && mb_strstr($title,'--') && !mb_strstr($title, 'unspecified products') )
			{
				$is_smth_found = true;
				$addition = array();
				
				/* Specific search */
				$exact_html = file_get_html($base_url.urlencode($title));
				foreach($exact_html->find("img") as $exact_img)
					array_push($addition, $exact_img);
					
				$bundle = array_merge($addition, $bundle);	
			}
		}
			
		return array('key'=>'wolphram', 'data'=>$bundle, 'estimation'=>count($bundle));
		
		}	
	}

?>