<?php

class Mail extends BasicData
{
	private static $bundle = array();
	private static $user_question = NULL;
	
	public static function getData($query)
	{		
		$base_url = "http://go.mail.ru/api/v1/web_search?fm=1&q=";
		$url = $base_url.urlencode($query);
		$data = http_request($url, FALSE, '', 'http://go.mail.ru', FALSE, 30);
		
		$data = json_decode($data);
		$data = $data->body->serp->results;
		
		/* Api result debug */
		//echo '<pre>'; print_r($data); echo '</pre><hr>';
		//return;
		
		Mail::$bundle = array();
		Mail::$user_question = $query;
		
		foreach($data as $answer)
		{
			if(isset($answer->smack_type) && trim(mb_strtolower($answer->smack_type)) === "answer")
				Mail::ProcessAnswersBunch($answer->results);
			else
				Mail::ProcessAnswer($answer);
		}
		
		uasort(Mail::$bundle, 'Mail::BundleSort');
		
		$origBundle = array();
		$fwdBundle = array();
		$estimation = 0;
		
		foreach(Mail::$bundle as $subj)
		{
			if($subj->similarity < 70.0/*%*/)
				continue;
				
			if($subj->isMail)
				array_push($fwdBundle, $subj);
			else
				array_push($origBundle, $subj->answer);	
				
			$estimation++;	
		}
		
		return array('key'=>'mail', 'data'=>$origBundle, "forward"=>$fwdBundle, 'estimation'=>$estimation);
		//echo '<pre>'; print_r(Mail::$bundle); echo '</pre><hr>';
		
	}
	
	private static function ProcessAnswersBunch($answersBunch)
	{
		foreach($answersBunch as $answer)
			Mail::ProcessMailAnswer($answer);	
	}
	
	private static function ProcessAnswer($answer)
	{
		//echo '<pre>'; print_r($answer); echo($answer->title); echo '</pre><hr>';
		if(!isset($answer->title) || !isset($answer->passage))
			return;
		
		$title = strip_tags($answer->title);
		$passage = $answer->passage;
		
		$similarity = 0;
		similar_text(
			Mail::CanonizeResultTitle(Mail::$user_question), 
			Mail::CanonizeResultTitle($title), 
			$similarity
			);
		
		$answObj = new StdClass();
		$answObj->title = $title;
		$answObj->answer = $passage;
		$answObj->similarity = $similarity;
		$answObj->isMail = false;
		
		array_push(Mail::$bundle, $answObj);
		
	}
	
	private static function ProcessMailAnswer($answer)
	{
		//echo '<pre>'; print_r($answer); echo($answer->title); echo '</pre><hr>';
		if(!isset($answer->question))
			return;
		
		$url = $answer->url;
		$question = strip_tags($answer->question);
		
		$passage = NULL;
		if(isset($answer->banswer))
			$passage = $answer->banswer;
		else if(isset($answer->answer))
			$passage = $answer->answer;
		else return;
		
		$similarity = 0;
		similar_text(
			Mail::CanonizeResultTitle(Mail::$user_question), 
			Mail::CanonizeResultTitle($question), 
			$similarity
			);
		
		$mailObj = new StdClass();
		$mailObj->url = $url;
		$mailObj->question = $question;
		$mailObj->similarity = $similarity;
		$mailObj->isMail = true;
		
		array_push(Mail::$bundle, $mailObj);
		
	}
	
	private static function BundleSort($a, $b)
	{
		if(!isset($a->similarity) || !isset($b->similarity))
			return -2;
			
		if($a->similarity == $b->similarity)
			return 0;
			
		return $a->similarity > $b->similarity ? -1 : 1;
	}
	
	private static function CanonizeResultTitle($title)
	{
		$title = trim(mb_strtolower($title));
		$title = explode("|", $title)[0];
		$title = explode("-", $title)[0];
		
		$title = str_replace(array('!','?','.',','), array('','','',''), $title);
		
		return $title;
	}
	
}

?>