<?php

class Nigma extends BasicData
{	
	public static function getData($query)
	{
		$url = 'http://pda.nigma.ru/?t=web&s='.urlencode($query);	
		$cookie = "spam_captcha=0d2f9821fde7c71eba505e519e53caad;";
		
		$data = http_request($url, FALSE, '', 'http://pda.nigma.ru/', $cookie, 30, FALSE);		
		$html = new simple_html_dom();
		$html->load($data);
		
		/* If answer contains images */
		foreach($html->find("img") as $img)
		{
			$make_absolute_link = !mb_strstr($img->attr['src'], 'http://');
			if($make_absolute_link)
				$img->attr['src'] = 'http://www.nigma.ru/'.$img->attr['src'];
		}
			
		/* Clean garbage */
		foreach($html->find(".q_more_spo") as $more)
			$more->innertext="";
		foreach($html->find(".src_block") as $src)
			$src->innertext="";
			
		
		function getChild($element, $child)
		{
			$el = array();
			if( is_array($element) && count($element)>0 ){
				$elements = $element[0]->find($child);
				if(count($elements)>0) $el = array($elements[0]);
			}
			return $el;
			}
			
		/* Merging all possible results */
		$board = array_merge( 
			$html->find("#board_pda > .resh_item:eq(0)"), 
			$html->find(".blocknum_1"),
			$html->find(".react_box"),
			$html->find(".reply_text")
		);
			
		$estimation = 0;
		
		/* if something was found */
		if(count($board)>0)
		{
		    $board = implode('<br><hr><br>',$board);
		    $board = mb_substr(strip_tags($board,'<img><i><br><b><p><sup><sub><h3><h2><h4>'),0,5000);	
					
			if( mb_strstr($board,'Варианты запроса :') ) 
				$board='';
						
			$board = Utils::RemoveTrash($board, array(
				'Введите e-mail',
				'Какой вариант решения Вы искали?',
				'Отменить',
				'Скрыть',
				'Пожаловаться',
				'http://www.nigma.ru/themes/nigma/img/chem/react.png',
				'http://www.nigma.ru/themes/nigma/img/chem/cond.png',
				'http://www.nigma.ru/themes/nigma/img/chem/ion.png',
				'почему происходят химические реакции',
				'Показать, как уравнять эту реакцию',
				'(Вычисление)',
				'Дано:',
				'<i>2000</i>',
				'Больше ответов',
				'Больше реакций',
				'Какой ответ Вы ожидали?',
				'Осталось символов: 2000',
				'Меньше реакций',
				'Ответы',
				'подробнее',
				'Ответ&nbsp;:',
				'Ответ:',
				'Не довольны результатом?',
				'Мы постараемся Вам помочь',
				'Введите Ваш e-mail',
				'Какой ответ Вы хотели получить?'
			));

			
			$board = trim($board);
			$estimation = 1;
		}
		else{
			$board = '';
		}
			
			
		return array('key'=>'nigma', 'data'=>array($board), 'estimation'=>$estimation);
		
		
		}	
	}

?>